# Changelog

All notable changes to this project will be documented in this file.  
Roadmap and upcoming features will be documented in this file.

---

## Upcoming Features

* Management of ansible service user in `users` role.
* Role `firewall`.
* Role `nginx`.
* Role `vault`.
---

## Version 1.0.0 (TBA)

### Features

* Role `ssh` added for management of SSH daemon.
* Role `users` added for management of users.
* Role `certificates` added for management of certificates.
* Role `ntp` added for management of chrony.

### Bugfixes

* None.

---
