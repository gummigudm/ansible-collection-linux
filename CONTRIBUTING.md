# Contributing

## Issues

Feel free to 
[file an issue](https://gitlab.com/gummigudm/ansible-collection-linux/-/issues) 
if you notice bugs or you think reworks are needed.

## Contributions

To contribute features or bugfixesplease submit a 
[merge request](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html)
.

## Guidelines

Simple guidelines for merge requests:

* Use 
[Ansible best practices](https://docs.ansible.com/ansible/latest/user_guide/playbooks_best_practices.html) 
when relevant.
* Lint and follow coding standards set in `.yamllint` and `.ansible-lint`
* Perform local molecule tests to make sure project retains all capabilites.
