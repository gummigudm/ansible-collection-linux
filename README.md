# Ansible Collection - gummigudm.linux

Ansible collection for linux provisioning.  
Can perform various hardening actions, set up software and configure servers.

<br>

## Table of Contents
* [Information](#information)
* [Installation](#installation)
* [Usage](#usage)
* [Development](#development)
* [Changes](#changes)
* [Contributing](#contributing)
* [License](#license)
* [Contact](#contact)

<br>

## Information

### Platforms
Content specific platforms depicted in `docs` folder.  
Most content is though supported by the following list.  
Backwards compatability not guaranteed but likely supported.
  * Centos Stream 8
  * Debian 10

### Requirements
Collection only uses common modules, so ansible core is the only requirement.
  * Ansible 2.9.11+

### Dependencies
Collection works independently
* *None*

<br>

## Installation

### Installation instructions

1. Create directory:
    ``` bash
    mkdir -p ~/Code/provision/resources/ansible_collections/gummigudm
    cd ~/Code/provision/resources/ansible_collections/gummigudm
    ```
2. Clone collection:
    ``` bash
    git clone https://gitlab.com/gummigudm/ansible-collection-linux.git linux
    ```
3. Add directory to Ansible config file:
    ``` bash
    # Ansible configuration file
    ...
    collections_paths = ~/Code/provision/resources
    roles_path = ~/Code/provision/resources
    ...
    ```
4. Incorporate in playbooks as specified in [usage section](#usage).

<br>

## Usage

Detailed collection documentation can be found in `docs` folder.

### Examples
Example `playbook.yml`.

``` yaml
- hosts: servers
  vars:
    ssh_config_passwordauthentication: no
    ssh_config_pubkeyauthentication: yes
    ssh_config_permitrootlogin: no
    user_list:
      - name: user1
        state: present
      - name: user2
        state: present
  roles:
    - gummigudm.linux.ssh
    - gummigudm.linux.users
```

<br>

## Development

### Local Development
Development can be perfomed locally using Docker or Vagrant.  
The collection is tested using Molecule.

#### Installation:
1. [Install collection](#installation).
2. [Install Podman](https://formulae.brew.sh/formula/podman).
3. Setup virtual environment in project directory:
    ``` bash
    cd ~/Code/provision/resources/ansible_collections/gummigudm/linux
    python3 -m venv .venv
    source .venv/bin/activate
    pip install --upgrade pip
    pip install pipenv
    ```
4. Install Python packages:
    ``` bash
    pipenv install
    ```
5. Deactivate virtual environment:
    ``` bash
    deactivate
    ```

#### Usage:
1. Develop content locally.
2. Test content:
    ``` bash
    cd ~/Code/provision/resources/ansible_collections/gummigudm/linux
    source .venv/bin/activate
    molecule test -s role_name
    ```
3. Share changes according to [contributing](#contributing) guidelines.

### Gitlab Pipeline
  * TODO: Tag, pipeline, etc.

<br>

## Changes

Version history with features and bugfixes, as well as upcoming features and roadmap  
depicted in `CHANGELOG.md`

<br>

## Contributing

Any contributions are greatly appreciated. See `CONTRIBUTING.md` for more information.

### Contributors

* [gummigudm](https://gitlab.com/gummigudm)  

<br>

## License

Distributed under the MIT License. See `LICENSE` for more information.

<br>

## Contact

Guðmundur Guðmundsson - gummigudm@gmail.com

* Gitlab - [gummigudm](https://gitlab.com/gummigudm)  
* Github - [gummigudm](https://github.com/gummigudm)  
