# Role - gummigudm.linux.access

Ansible role for provisioning users and groups.  
Adds and removes users and groups, manages sudoers permissions, known hosts and authorized keys.

<br>

## Table of Contents
* [Information](#information)
* [Usage](#usage)

<br>

## Information

### Platforms
Tests in place for listed platforms. Backwards compatability not guaranteed but likely supported.
  * Centos Stream 8
  * Debian 10

<br>

## Usage

### Examples
Example `playbook.yml`.

``` yaml
- hosts: servers
  vars:
    user_list:
      - name: user1
        state: present
        expires: never
        data:
          id: 2001
          comment: User 1
          shell: bash
          home: default
        auth:
          groups: ['group1','group2']
          sudo: ['ALL=(ALL:ALL) NOPASSWD:ALL']
          authorized_keys: ['authorizedkey1']
          known_hosts: ['knownhost1','knownhost2']
          purge_known_hosts: false
          password: '!'
          update_password: always

    user_list_named:
      - name: user2
        expires: "2022-04-17"
        ssh:
          known_hosts: ['knownhost1','knownhost2']
          public_keys: ['authorizedkey1','authkey2']
      - name: user3
        data:
          shell: default
        state: absent

    group_list:
      - name: group1
        data:
          id: 3001

    group_list_named:
      - name: group2
  roles:
    - gummigudm.linux.users
```

### Tags
The table contains tags available for role.

| Tag            | Purpose/tasks |
| -------------- | ------------- |
| access         | Global tag for complete role.

### Variables
The tables contains all variables usable by the role.

#### Settings

| Variable                                     | Default                   | Type    | Description |
| -------------------------------------------- | ------------------------- | ------- |------------ |
| *Role settings*                              |
| `access_role_enabled`                         | true                      | bool    | Toggle role.
| `access_settings_manage_root_user`            | false                     | bool    | Permit editing of root user.
| `access_settings_manage_provision_user`       | false                     | bool    | Permit editing of provision user.

#### Configuration

| Variable                                     | Default                   | Required | Type    | Description |
| -------------------------------------------- | ------------------------- | -------- | ------- |------------ |
| *Role configuration*                         |
| `user_list`                                  | []                        |          | list    | List of users. Details depicted below.
| `group_list`                                 | []                        |          | list    | List of groups. Details depicted below.
| `user_list_*`                                | []                        |          | list    | Named list of users. Details depicted below.
| `group_list_*`                               | []                        |          | list    | Named list of groups. Details depicted below.

#### User list

The `user_list` and `user_list_*` variables takes a list of dictionaries with parameters for users.  
Detailed information of configuration variables can be found under [useradd(8)](https://linux.die.net/man/8/useradd) and [ansible.builtin.user](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/user_module.html).

| Variable                                     | Default                   | Required | Type    | Description |
| -------------------------------------------- | ------------------------- | -------- | ------- |------------ |
| *`user_list[]`*                              |
| `.name`                                      |                           | yes      | string  | Users name.
| `.state`                                     | present                   | no       | string  | Create remove or disable user. [present/absent/disabled]
| `.expires`                                   | never                     | no       | string  | Set user expiry date. [never/yyyy-mm-dd]
| `.data`                                      |                           | no       | dict    | User data.
| &emsp;`.id`                                  |                           | no       | integer | Users UID.
| &emsp;`.comment`                             |                           | no       | string  | Users comment.
| &emsp;`.shell`                               | default                   | no       | string  | Users shell or `nologin`. Shell must be installed.
| &emsp;`.home`                                | default                   | no       | string  | Users home directory or `default` or `none`.
| `.access`                                    |                           | no       | dict    | User access.
| &emsp;`.groups`                              | []                        | no       | list    | Users groups.
| &emsp;`.sudo`                                | []                        | no       | list    | List of permissions in sudoers file.
| `.password`                                  |                           | no       | dict    | User password.
| &emsp;`.hash`                                | '!'                       | no       | string  | Users password or `'!'` for none.
| &emsp;`.update`                              | always                    | no       | string  | Users password or `'!'` for none.
| `.ssh`                                       |                           | no       | dict    | User ssh.
| &emsp;`.host_keys`                           | []                        | no       | list    | Inclusive list of known hosts / dict.
| &emsp;&emsp;`.known`                         | []                        | no       | list    | Inclusive list of known hosts.
| &emsp;&emsp;`.purge`                         | false                     | no       | list    | Purges known hosts.
| &emsp;`.public_keys`                         | []                        | no       | list    | Definitive list of authorized keys / dict.
| &emsp;&emsp;`.authorized`                    | []                        | no       | list    | Definitive list of authorized keys.
| &emsp;`.private_key`                         |                           | no       | dict    | Users public key, generated or custom.
| &emsp;&emsp;`.state`                         | present                   | no       | string  | Private key state. Default present if private_key is defined.
| &emsp;&emsp;`.name`                          | id_type                   | no       | string  | Private key name. Required if custom key defined.
| &emsp;&emsp;`.key`                           |                           | no       | string  | Private key content. If not defined, key will be generated. 
| &emsp;&emsp;`.type`                          | ed25519                   | no       | string  | Private key type. [ed_25519/rsa]
| &emsp;&emsp;`.comment`                       | ''                        | no       | string  | Private key comment.
| &emsp;&emsp;`.authorized`                    | false                     | no       | bool    | Add generated public key to authorized_keys.
| &emsp;&emsp;`.export`                        | false                     | no       | bool    | Export generated keys to variable `exported_ssh_keys`.

#### Group list

The `group_list` and `group_list_*` variables takes a list of dictionaries with parameters for groups.  
Detailed information of configuration variables can be found under [groupadd(8)](https://linux.die.net/man/8/groupadd) and [ansible.builtin.group](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/group_module.html).

| Variable                                     | Default                   | Required | Type    | Description |
| -------------------------------------------- | ------------------------- | -------- | ------- |------------ |
| *`group_list[]`*                             |
| `.name`                                      |                           | yes      | string  | Groups name.
| `.state`                                     | present                   | no       | string  | Create or remove group. [present/absent]
| `.data`                                      |                           | no       | dict    | Group details.
| &emsp;`.id`                                  |                           | no       | integer | Groups GID.
| `.access`                                    |                           | no       | dict    | Groups access.
| &emsp;`.sudo`                                |                           | no       | list    | List of permissions in sudoers file.
