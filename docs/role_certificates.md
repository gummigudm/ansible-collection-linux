# Role - gummigudm.linux.certificates

Ansible role for distributing ssl certificates and keys.  
Creates directories, adds updates and removes certificates and keys.

<br>

## Table of Contents
* [Information](#information)
* [Usage](#usage)

<br>

## Information

### Platforms
Tests in place for listed platforms. Backwards compatability not guaranteed but likely supported.
  * Centos Stream 8
  * Debian 10

<br>

## Usage

### Examples
Example `playbook.yml`.

``` yaml
- hosts: servers
  vars:
    certificates_config_certificates:
      - name: "*.certificate.com"
        state: present
        group: root
        certificate: |
          -----BEGIN CERTIFICATE-----
          ...
          certificate contents
          ...
          -----END CERTIFICATE-----
          -----BEGIN CERTIFICATE-----
          ...
          ca bundle contents
          ...
          -----END CERTIFICATE-----
        key: |
          -----BEGIN PRIVATE KEY-----
          ...
          private key contents
          ...
          -----END PRIVATE KEY-----

  roles:
    - gummigudm.linux.certificates
```

### Tags
The table contains tags available for role.

| Tag            | Purpose/tasks |
| -------------- | ------------- |
| certificates   | Global tag for complete role.

### Variables
The tables contains all variables usable by the role.

#### Settings

| Variable                                     | Default                   | Type    | Description |
| -------------------------------------------- | ------------------------- | ------- |------------ |
| *Role settings*                              |
| `certificates_role_enabled`                  | true                      | bool    | Toggle role.

#### Configuration

| Variable                                     | Default                   | Required | Type    | Description |
| -------------------------------------------- | ------------------------- | -------- | ------- |------------ |
| *Role configuration*                         |
| `certificates_config_directory`              | Distro specific           |          | list    | Directory to store certificates.
| `certificates_config_certificates`           | []                        |          | list    | List of certificates. Details depicted below.

#### Certificates list

The `certificates_config_certificates` variable takes a list of dictionaries with parameters for certificates.

| Variable                                     | Default                   | Required | Type    | Description |
| -------------------------------------------- | ------------------------- | -------- | ------- |------------ |
| *`certificates_config_certificates[]`*       |
| `.name`                                      |                           | yes      | string  | Certificate domain name.
| `.state`                                     | present                   | no       | string  | Distribute or remove certificate. [present/absent]
| `.group`                                     | root                      | no       | string  | Group to own file.
| `.certificate`                               |                           | yes      | string  | Certificate contents.
| `.key`                                       |                           | yes      | string  | Key contents.
