# Role - gummigudm.linux.ntp

Ansible role for managing network time protocol.  
Installs and configures ntp.

<br>

## Table of Contents
* [Information](#information)
* [Usage](#usage)

<br>

## Information

### Platforms
Tests in place for listed platforms. Backwards compatability not guaranteed but likely supported.
  * Centos Stream 8
  * Debian 10

<br>

## Usage

### Examples
Example `playbook.yml`.

``` yaml
- hosts: servers
  vars:
    ntp_config_source:
      pools:
        - source: "pool.ntp.org"
          options:
            - iburst
            - prefer
  roles:
    - gummigudm.linux.ntp
```

### Tags
The table contains tags available for role.

| Tag            | Purpose/tasks |
| -------------- | ------------- |
| ntp            | Global tag for complete role.

### Variables
The tables contains all variables usable by the role.  
No variables required for default configuration.

#### Settings

| Variable                                     | Default                   | Type    | Description |
| -------------------------------------------- | ------------------------- | ------- |------------ |
| *Role settings*                              |
| `ntp_role_enabled`                           | true                      | bool    | Toggle role.
| `ntp_settings_validate_servers`              | false                     | bool    | Validate ping connectivity of servers (requires ping).
| `ntp_settings_validate_peers`                | false                     | bool    | Validate ping connectivity of peers (requires ping).

#### Configuration

| Variable                                     | Default                   | Required | Type    | Description |
| -------------------------------------------- | ------------------------- | -------- | ------- |------------ |
| *Role configuration*                         |
| `ntp_config_source`                          |                           | no       | dict    | Dictionary for client. Details depicted below.
| `ntp_config_server`                          |                           | no       | dict    | Dictionary for server. Details depicted below.
| `ntp_config_keys`                            | []                        | no       | list    | List of keys.
| `ntp_config_options`                         | [rtcsync,makestep 1.0 3]  | no       | list    | List of [options](https://chrony.tuxfamily.org/doc/3.4/chrony.conf.html).
| `ntp_config_user`                            | Distro specific           | no       | string  | Chrony service user (user must exist).

#### Source lists

The `ntp_config_source` variable is a Dictionary with parameters for sources.  
Detailed information of options can be found under [chrony.conf(5)](https://chrony.tuxfamily.org/doc/3.4/chrony.conf.html)

| Variable                                     | Default                   | Required | Type    | Description |
| -------------------------------------------- | ------------------------- | -------- | ------- |------------ |
| *`ntp_config_source`*                        |
| `.pools[]`                                   |                           | no       | list    | List of pools to fetch time info (Client).
| &emsp;`.source`                              | pool.ntp.org              | no       | string  | Domain name or IP of pool.
| &emsp;`.options`                             | [iburst,prefer]           | no       | list    | Pool [options](https://chrony.tuxfamily.org/doc/3.4/chrony.conf.html).
| `.servers[]`                                 |                           | no       | list    | List of servers to fetch time info (Client).
| &emsp;`.source`                              |                           | no       | string  | Domain name or IP of server.
| &emsp;`.options`                             |                           | no       | list    | Server [options](https://chrony.tuxfamily.org/doc/3.4/chrony.conf.html).

#### Server lists

The `ntp_config_server` variable is a Dictionary with parameters for servers.  
Detailed information of options can be found under [chrony.conf(5)](https://chrony.tuxfamily.org/doc/3.4/chrony.conf.html)

| Variable                                     | Default                   | Required | Type    | Description |
| -------------------------------------------- | ------------------------- | -------- | ------- |------------ |
| *`ntp_config_server`*                        |
| `.peers[]`                                   |                           | no       | list    | List of peers for pool (Server).
| &emsp;`.source`                              |                           | no       | string  | Domain name or IP of peer.
| &emsp;`.options`                             |                           | no       | list    | Peer [options](https://chrony.tuxfamily.org/doc/3.4/chrony.conf.html).
| `.allows[]`                                  |                           | no       | list    | List of allowed IPs or CIDR ranges (Server).
| &emsp;`.source`                              |                           | no       | string  | IP or CIDR of allow.
| &emsp;`.options`                             |                           | no       | list    | Allow [options](https://chrony.tuxfamily.org/doc/3.4/chrony.conf.html).
