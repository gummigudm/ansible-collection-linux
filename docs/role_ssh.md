# Role - gummigudm.linux.ssh

Ansible role for provisioning SSH.  
Configures SSH daemon and manages host keys.

<br>

## Table of Contents
* [Information](#information)
* [Usage](#usage)

<br>

## Information

### Platforms
Tests in place for listed platforms. Backwards compatability not guaranteed but likely supported.
  * Centos Stream 8
  * Debian 10

<br>

## Usage

### Examples
Example `playbook.yml`.

``` yaml
- hosts: servers
  vars:
    ssh_config_passwordauthentication: false
    ssh_config_pubkeyauthentication: true
    ssh_config_permitrootlogin: false
  roles:
    - gummigudm.linux.ssh
```

### Tags
The table contains tags available for role.

| Tag            | Purpose/tasks |
| -------------- | ------------- |
| ssh            | Global tag for complete role.

### Variables
The tables contains all variables usable by the role.  
No variables are required for default configuration.

#### Settings

| Variable                                     | Default                   | Type    | Description |
| -------------------------------------------- | ------------------------- | ------- |------------ |
| *Role settings*                              |
| `ssh_role_enabled`                           | true                      | bool    | Toggle role.

#### Configuration

| Variable                                     | Default                   | Type    | Description |
| -------------------------------------------- | ------------------------- | ------- |------------ |
| *Connection*                                 |
| `ssh_config_port`                            | ['22']                    | list    | Specifies the ports that the daemon listens on.
| `ssh_config_addressfamily`                   | inet                      | string  | Specifies address family to use.
| `ssh_config_listenaddress`                   | ['0.0.0.0']               | list    | Specifies the local addresses to listen on.
| `ssh_config_rdomain`                         | none                      | string  | Specifies an explicit routing domain that is applied after authentication.
| *Global Authentication*                      |
| `ssh_config_authenticationmethods`           | publickey,password        | string  | Specifies available authentication methods.
| `ssh_config_challengeresponseauthentication` | true                      | bool    | Permits challenge-response authentication.
| `ssh_config_permitrootlogin`                 | false                     | bool    | Permits root login over ssh.
| `ssh_config_usepam`                          | true                      | bool    | Enables the pluggable authentication module interface.
| *Password Authentication*                    |
| `ssh_config_passwordauthentication`          | true                      | bool    | Permits password authentication.
| `ssh_config_permitemptypasswords`            | false                     | bool    | Permits connections of users with an empty password.
| `ssh_config_kbdinteractiveauthentication`    | false                     | bool    | Permits keyboard-interactive authentication.
| *Public Key Authentication*                  |
| `ssh_config_pubkeyauthentication`            | true                      | bool    | Permits public key authentication.
| `ssh_config_authorizedkeysfile`              | .ssh/authorized_keys      | string  | Specifies file that contains authorized public keys.
| `ssh_config_trustedusercakeys`               | none                      | string  | Specifies file containing public keys for all users.
| `ssh_config_revokedkeys`                     | none                      | string  | Specifies file that contains revoked public keys.
| `ssh_config_authorizedkeyscommand`           | none                      | string  | Specifies program to authenticate authorized keys.
| `ssh_config_authorizedkeyscommanduser`       | none                      | string  | Specifies user to run authorized keys command.
| `ssh_config_authorizedprincipalscommand`     | none                      | string  | Specifies program to generate list of certificate principals.
| `ssh_config_authorizedprincipalscommanduser` | none                      | string  | Specifies user to run authorized principals command.
| `ssh_config_authorizedprincipalsfile`        | none                      | string  | Specifies file that lists accepted prinicipal names.
| *Kerberos Authentication*                    |
| `ssh_config_kerberosauthentication`          | false                     | bool    | Permits Kerberos authentication.
| `ssh_config_kerberosorlocalpasswd`           | false                     | bool    | Specifies if Kerberos should fallback to local password.
| `ssh_config_kerberosgetafstoken`             | false                     | bool    | Specifies if AFS token should be aquired.
| `ssh_config_kerberosticketcleanup`           | false                     | bool    | Specifies whether Kerberos ticket cache will be destroyed upon logout.
| *GSSAPI Authentication*                      |
| `ssh_config_gssapiauthentication`            | false                     | bool    | Permits GSSAPI authentication.
| `ssh_config_gssapicleanupcredentials`        | false                     | bool    | Specifies if GSSAPI user credential cache is destroyed on logout.
| `ssh_config_gssapistrictacceptorcheck`       | false                     | bool    | Restrict GSSAPI authentocation to hostname.
| *Host Based Authentication*                  |
| `ssh_config_hostbasedauthentication`         | false                     | bool    | Permits host based authentication.
| `ssh_config_hostbasedusesnamefrompacketonly` | false                     | bool    | Perform reverse lookup when maching host based authentication.
| `ssh_config_ignorerhosts`                    | true                      | bool    | Ignore rhosts and shosts for host based authentication.
| `ssh_config_ignoreuserknownhosts`            | true                      | bool    | Ignore known_hosts for host based authentication.
| `ssh_config_hostkeyagent`                    | none                      | string  | Specifies socket to connect to agent with access to private host keys.
| `ssh_config_hostcertificate`                 | none                      | string  | Specifies certificate to match host key.
| *Print*                                      |
| `ssh_config_printlastlog`                    | false                     | bool    | Specifies if last login should be printed.
| `ssh_config_printmotd`                       | false                     | bool    | Specifies if message of the day file should be printed.
| `ssh_config_banner`                          | none                      | string  | Specifies file to display banner from before login.
| `ssh_config_versionaddendum`                 | none                      | string  | Specifies additional banner text to print.
| *Permissions*                                |
| `ssh_config_denyusers`                       | none                      | string  | Users denied from ssh.
| `ssh_config_allowusers`                      | none                      | string  | Users allowed to ssh.
| `ssh_config_denygroups`                      | none                      | string  | Groups denied from ssh.
| `ssh_config_allowgroups`                     | none                      | string  | Groups allowed to ssh.
| *Connection Settings*                        |
| `ssh_config_clientalivecountmax`             | 3                         | integer | Number of client alive messages without response.
| `ssh_config_clientaliveinterval`             | 0                         | integer | Interval of client alive messages.
| `ssh_config_logingracetime`                  | 120                       | integer | Specifies time to disconnect if no successful login attempt.
| `ssh_config_maxauthtries`                    | 6                         | integer | Specifies the maximum authentication attempts per connection.
| `ssh_config_maxsessions`                     | 10                        | integer | Maximum concurrent authenticated connections.
| `ssh_config_maxstartups`                     | '10:30:100'               | string  | Maximum concurrent unauthenticated connections.
| `ssh_config_tcpkeepalive`                    | false                     | bool    | Specifies if TCP keepalive messages should be sent.
| `ssh_config_rekeylimit`                      | default none              | string  | Specifies if data or time based rekeying should occurr.
| *Settings*                                   |
| `ssh_config_loglevel`                        | INFO                      | string  | Specifies log level of daemon.
| `ssh_config_syslogfacility`                  | AUTH                      | string  | Gives the facility code that is used when logging messages.
| `ssh_config_exposeauthinfo`                  | false                     | bool    | Exposes authentication methods via SSH_USER_AUTH variable.
| `ssh_config_usedns`                          | true                      | bool    | Specifies whether remote hostname should be checked for validation.
| `ssh_config_strictmodes`                     | true                      | bool    | Specifies whether file modes and ownership should be checked before login.
| `ssh_config_compression`                     | true                      | bool    | Specifies if sshd should use compression.
| *Environment*                                |
| `ssh_config_acceptenv`                       | ['']                      | list    | Specifies environment variable sent by client.
| `ssh_config_setenv`                          | ['']                      | list    | Specifies environment variables that override defaults.
| `ssh_config_permituserenvironment`           | false                     | bool    | Specifies if users environment should be processed. 
| `ssh_config_permituserrc`                    | true                      | bool    | Specifies if users rc file is sourced.
| `ssh_config_forcecommand`                    | none                      | string  | Forces run of command instead of .ssh/rc
| *Forwarding*                                 |
| `ssh_config_disableforwarding`               | false                     | bool    | Disables and overrides all forwarding features.
| `ssh_config_permittty`                       | true                      | bool    | Specifies whether pty allocation is permitted.
| `ssh_config_allowagentforwarding`            | true                      | bool    | Permits ssh-agent forwarding.
| `ssh_config_allowtcpforwarding`              | true                      | bool    | Permits TCP forwarding.
| `ssh_config_permittunnel`                    | false                     | bool    | Specifies whether tunnel device forwarding is permitted.
| `ssh_config_permitlisten`                    | ['']                      | list    | Specifies source addresses and ports where TCP port forwarding may listen.
| `ssh_config_permitopen`                      | ['']                      | list    | Specifies destination addresses and ports to which TCP port forwarding is permitted.
| `ssh_config_gatewayports`                    | no                        | string  | Permits remote hosts to connect to forwarded ports.
| `ssh_config_allowstreamlocalforwarding`      | true                      | bool    | Permits Unix-domain socket forwarding.
| `ssh_config_streamlocalbindmask`             | '0177'                    | string  | Specifies octal file creation mode mask when creating socket file.
| `ssh_config_streamlocalbindunlink`           | false                     | bool    | Specifies if socket file should be removed before creating new one.
| `ssh_config_x11forwarding`                   | true                      | bool    | Specifies whether X11 forwarding is permitted.
| `ssh_config_x11displayoffset`                | 10                        | integer | Specifies the first display number available for sshd(8)'s X11 forwarding.
| `ssh_config_x11uselocalhost`                 | true                      | bool    | Specifies whether to bind X11 to loopback or wildcard address.
| `ssh_config_xauthlocation`                   | /usr/local/bin/xauth      | string  | Specifies the full pathname of the xauth program.
| *System*                                     |
| `ssh_config_chrootdirectory`                 | none                      | string  | Specifies the pathname of a directory to chroot.
| `ssh_config_pidfile`                         | /var/run/sshd.pid         | string  | Specifies the process ID file.
| `ssh_config_ipqos`                           | af21 cs1                  | string  | Specifies the IPv4 type-of-service or DSCP class.
| `ssh_config_subsystem`                       | ['sftp internal-sftp']    | list    | Defines external subsystems.
| `ssh_config_useblacklist`                    | false                     | bool    | Specifies if authentication messages should be sent to blacklistd daemon.
| *Encryption*                                 |
| `ssh_config_hostkeys`                        | sshd default              | list    | Specifies host keys, with possibility of custom keys. Described below.
| `ssh_config_ciphers`                         | sshd default              | string  | Specifies the ciphers allowed for encryption.
| `ssh_config_macs`                            | sshd default              | string  | Specifies MAC algorithms.
| `ssh_config_kexalgorithms`                   | sshd default              | string  | Specifies key exchange algorithms.
| `ssh_config_hostkeyalgorithms`               | sshd default              | string  | Specifies host key algorithms.
| `ssh_config_pubkeyacceptedkeytypes`          | sshd default              | string  | Specifies accepted public key types.
| `ssh_config_hostbasedacceptedkeytypes`       | sshd default              | string  | Specifies host based authentication key types.
| `ssh_config_fingerprinthash`                 | sha256                    | string  | Specifies the algorithm for logging key fingerprints.

Detailed information of configuration variables can be found under [sshd_config(5)](https://linux.die.net/man/5/sshd_config) and [sshd(8)](https://linux.die.net/man/8/sshd).

#### Hostkeys list

The hostkeys list takes a list of dictionaries with the required keys `type` (ed25519,ecdsa,rsa) and `state` (present,renew,absent).  
For custom values, it can also take the optional keys `public_key` and `private_key`. If one is declaired, the other is also required.  
Example:

```
ssh_config_hostkeys:
  - type: ed25519
    state: present
    private_key: |
      -----BEGIN OPENSSH PRIVATE KEY-----
      b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAAAMwAAAAtzc2gtZW
      QyNTUxOQAAACDPmP2v8DlkVn7FyQ0sDtzyO+hUxCJwRn9JOcGddZEwBwAAAJjRRdEg0UXR
      IAAAAAtzc2gtZWQyNTUxOQAAACDPmP2v8DlkVn7FyQ0sDtzyO+hUxCJwRn9JOcGddZEwBw
      AAAEDmM3HUuXTlfMBhEVz15UVI3TDv2fNZWTlAgY8pJlpf88+Y/a/wOWRWfsXJDSwO3PI7
      6FTEInBGf0k5wZ11kTAHAAAAEXJvb3RAMWY1MGQxNWE3MDY5AQIDBA==
      -----END OPENSSH PRIVATE KEY-----
    public_key: ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIM+Y/a/wOWRWfsXJDSwO3PI76FTEInBGf0k5wZ11kTAH
  - type: ecdsa
    state: renew
  - type: rsa
    state: absent
```
