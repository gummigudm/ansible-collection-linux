#!/usr/bin/python
# Filter plugins for data manipulation

from datetime import datetime

from ansible.errors import AnsibleError

#from ansible.module_utils.common.text.converters import to_native

class FilterModule(object):
    def filters(self):
        return {
            'access_parse_users': self.parse_users,
            'access_parse_groups': self.parse_groups,
            'access_add_root_user': self.add_root_user,
            'access_shell_packages': self.shell_packages,
            'access_needs_ssh_packages': self.needs_ssh_packages,
            'access_users_public_key': self.users_public_key,
            'access_combine_export_keys': self.combine_export_keys
        }

    # Return parsed user list if validation passes
    def parse_users(self, user_list, prov_user_name ,manage_prov, parsing_vars):
        names = set()
        ids = set()
        users = []
        # Add users to list
        for user_item in user_list:
            user = {}
            user['type'] = 'user'
            user['name'] = ''
            user['group'] = ''
            user['expires'] = float(-1)
            user['state'] = 'present'
            user['id'] = ''
            user['comment'] = ''
            user['shell'] = ''
            user['home'] = ''
            user['home_create'] = True
            user['home_move'] = True
            user['groups']=''
            user['sudo_permissions'] = []
            user['password'] = '!'
            user['password_update'] = 'always'
            user['ssh_directory'] = False
            user['ssh_known_hosts'] = []
            user['ssh_known_hosts_purge'] = False
            user['ssh_authorized_keys'] = []
            user['ssh_private_key_state'] = 'absent'
            user['ssh_private_key_generate'] = False
            user['ssh_private_key_content'] = ''
            user['ssh_private_key_type'] = ''
            user['ssh_private_key_bits'] = ''
            user['ssh_private_key_comment'] = ''
            user['ssh_private_key_file'] = ''
            user['ssh_private_key_path'] = ''
            user['ssh_private_key_authorized'] = False
            user['ssh_private_key_export'] = False

            # Name & Group
            if 'name' not in user_item:
                raise AnsibleError('User field missing: name')
            if not user_item['name']:
                raise AnsibleError('User field empty: name')
            if user_item['name'] == 'root':
                raise AnsibleError('User root can not be defined in list. Use: access_root_password')
            if user_item['name'] == prov_user_name:
                if not manage_prov:
                    continue
            if user_item['name'] in names:
                 raise AnsibleError('User field must be unique: name')

            names.add(user_item['name'])
            user['name'] = user_item['name']
            user['group'] = user_item['name']

            # Expiry
            if 'expires' in user_item and user_item['expires']:
                if user_item['expires'] == 'never':
                    user['expires'] = float(-1)
                else:
                    dt = user_item['expires']
                    user['expires'] = datetime.strptime(dt, "%Y-%m-%d").timestamp()

            # State
            if 'state' in user_item:
                if user_item['state'] not in ['present','absent','disabled']:
                    raise AnsibleError('User state can only be present, absent or disabled')
                if user_item['state'] == 'disabled':
                    user['state'] = 'present'
                    user['expires'] = float(1)
                else:
                    user['state'] = user_item['state']

            # Data
            if 'data' in user_item:
                # UID
                if 'id' in user_item['data']:
                    if user_item['data']['id'] in ids:
                        raise AnsibleError('User field must be unique: id')
                    else:
                        if 100 <= user_item['data']['id'] <= 60000:
                            ids.add(user_item['data']['id'])
                            user['id'] = user_item['data']['id']
                        else:
                            raise AnsibleError('User id must be an integer between 100-60000')

                # Comment
                if 'comment' in user_item['data'] and user_item['data']['comment']:
                    user['comment'] = user_item['data']['comment']

                # Shell
                if 'shell' in user_item['data'] and user_item['data']['shell']:
                    shell = user_item['data']['shell']
                    if user_item['data']['shell'] in parsing_vars['shells']['installed']:
                        user['shell'] = parsing_vars['shells']['installed'][shell]
                    elif user_item['data']['shell'] in parsing_vars['shells']['uninstalled']:
                        user['shell'] =  parsing_vars['shells']['uninstalled'][shell][0]
                    else:
                       raise AnsibleError('User shell value unsupported')

                # Home
                if 'home' in user_item['data'] and user_item['data']['home']:
                    if user_item['data']['home'] == 'none':
                        user['home_create'] = False
                    elif user_item['data']['home'] == 'default':
                        user['home_create'] = True
                        user['home_move'] = True
                    else:
                        user['home'] = user_item['data']['home']
                        user['home_create'] = True
                        user['home_move'] = True

            # Groups
            if 'access' in user_item and 'groups' in user_item['access']:
                for i in range(len(user_item['access']['groups'])):
                    if user_item['access']['groups'][i] in parsing_vars['groups']['admin']:
                        user_item['access']['groups'][i] = parsing_vars['groups']['admin'][0]
                user['groups'] = ",".join(user_item['access']['groups'])

            #  Sudo
            if 'access' in user_item and 'sudo' in user_item['access']:
                user['sudo_permissions'] = user_item['access']['sudo']

            # Password
            if 'password' in user_item and 'hash' in user_item['password'] and user_item['password']['hash']:
                user['password'] = user_item['password']['hash']
            if 'password' in user_item and 'update' in user_item['password']:
                if not user_item['password']['update']:
                    user['password_update'] = 'onchange'

            # SSH
            if 'ssh' in user_item:
                if not user['home_create']:
                    raise AnsibleError('Home must be created for ssh directory')
                user['ssh_directory'] = True

                # Known hosts
                if 'host_keys' in user_item['ssh']:
                    if isinstance(user_item['ssh']['host_keys'], list):
                        user['ssh_known_hosts'] = user_item['ssh']['host_keys']
                    else:
                        if 'known' in user_item['ssh']['host_keys']:
                            user['ssh_known_hosts'] = user_item['ssh']['host_keys']['known']
                        if 'purge' in user_item['ssh']['host_keys'] and user_item['ssh']['host_keys']['purge']:
                            user['ssh_known_hosts_purge'] = True

                # Authorized keys
                if 'public_keys' in user_item['ssh']:
                    if isinstance(user_item['ssh']['public_keys'], list):
                        user['ssh_authorized_keys'] = user_item['ssh']['public_keys']
                    else:
                        if 'authorized' in user_item['ssh']['public_keys']:
                            user['ssh_authorized_keys'] = user_item['ssh']['public_keys']['authorized']

                # Private key
                if 'private_key' in user_item['ssh']:
                    user['ssh_private_key_state'] = 'present'
                    user['ssh_private_key_generate'] = True
                    if 'state' in user_item['ssh']['private_key'] and user_item['ssh']['private_key']['state']:
                        if user['ssh_private_key_state'] in ['present','rotate','absent']:
                            user['ssh_private_key_state'] = user_item['ssh']['private_key']['state']
                        else:
                            raise AnsibleError('Private key state can only be present, absent or rotate')
                    if 'key' in user_item['ssh']['private_key'] and  user_item['ssh']['private_key']['key']:
                        user['ssh_private_key_content'] = user_item['ssh']['private_key']['key']
                        user['ssh_private_key_generate'] = False
                        if 'name' in user_item['ssh']['private_key'] and user_item['ssh']['private_key']['name']:
                            user['ssh_private_key_file'] = user_item['ssh']['private_key']['name']
                        else:
                            raise AnsibleError('Private key name must be declared for content')
                    else:
                        if user['ssh_private_key_state'] == 'absent':
                            user['ssh_private_key_generate'] = False
                        else:
                            user['ssh_private_key_generate'] = True
                        if 'type' in user_item['ssh']['private_key']:
                            if user_item['ssh']['private_key']['type'] in parsing_vars['ssh_key']['types']:
                                user['ssh_private_key_type'] = user_item['ssh']['private_key']['type']
                                if user['ssh_private_key_type'] == 'rsa':
                                    if 'bits' in user_item['ssh']['private_key']:
                                        if user_item['ssh']['private_key']['bits'] in parsing_vars['ssh_key']['bits']:
                                            user['ssh_private_key_bits'] = user_item['ssh']['private_key']['bits']
                                        else:
                                            raise AnsibleError('Private key bits value unsupported')
                                    else:
                                        user['ssh_private_key_bits'] = parsing_vars['ssh_key']['bits'][0]
                            else:
                                raise AnsibleError('Private key type value unsupported')
                        else:
                            user['ssh_private_key_type'] = parsing_vars['ssh_key']['types'][0]
                        if 'name' in user_item['ssh']['private_key'] and user_item['ssh']['private_key']['name']:
                            user['ssh_private_key_file'] = user_item['ssh']['private_key']['name']
                            user['ssh_private_key_path'] = parsing_vars['ssh_folder'] + '/' + user_item['ssh']['private_key']['name']
                        else:
                            user['ssh_private_key_file'] = 'id_' + user['ssh_private_key_type']
                            user['ssh_private_key_path'] = parsing_vars['ssh_folder'] + '/' + 'id_' + user['ssh_private_key_type']
                        if 'comment' in user_item['ssh']['private_key'] and user_item['ssh']['private_key']['comment']:
                            user['ssh_private_key_comment'] = user_item['ssh']['private_key']['comment']
                        if 'authorized' in user_item['ssh']['private_key'] and user_item['ssh']['private_key']['authorized']:
                            user['ssh_private_key_authorized'] = True
                        if 'export' in user_item['ssh']['private_key'] and user_item['ssh']['private_key']['export']:
                            user['ssh_private_key_export'] = True

            # Add item
            users.append(user)
        return users

    # Return parsed group list if validation passes
    def parse_groups(self, group_list, prov_user_name, manage_prov, users):
        names = set()
        ids = set()
        groups = []
        # Add groups to list
        for group_item in group_list:
            group = {}
            group['type'] = 'group'
            group['name'] = ''
            group['state'] = 'present'
            group['id'] = ''
            group['sudo_permissions'] = []

            # Name
            if 'name' not in group_item:
                raise AnsibleError('Group field missing: name')
            if not group_item['name']:
                raise AnsibleError('Group field empty: name')
            if group_item['name'] in ['root','wheel','sudo']:
                raise AnsibleError('Group root can not be defined in list')
            if group_item['name'] == prov_user_name:
                if not manage_prov:
                    continue
            if group_item['name'] in names:
                 raise AnsibleError('Group field must be unique: name')
            else:
                names.add(group_item['name'])
                group['name'] = group_item['name']

            # State
            if 'state' in group_item:
                if group_item['state'] not in ['present','absent']:
                    raise AnsibleError('Group state can only be present or absent')
                else:
                    group['state'] = group_item['state']

            # GID
            if 'data' in group_item and 'id' in group_item['data']:
                if group_item['data']['id'] in ids:
                    raise AnsibleError('Group field must be unique: id')
                else:
                    if 100 <= group_item['data']['id'] <= 60000:
                        ids.add(group_item['data']['id'])
                        group['id'] = group_item['data']['id']
                    else:
                        raise AnsibleError('Group id must be an integer between 100-60000')

            # Sudo
            if 'access' in group_item and 'sudo' in group_item['access']:
                group['sudo_permissions'] = group_item['access']['sudo']

            # Add item
            groups.append(group)

        # Add users groups to list
        for user_item in users:
            group = {}
            group['type'] = 'user group'
            group['name'] = ''
            group['id'] = ''
            group['state'] = 'present'
            group['sudo_permissions'] = []

            # Name
            if user_item['name'] in names:
                 raise AnsibleError('Group name already defined by user item')
            else:
                names.add(user_item['name'])
                group['name'] = user_item['name']

            # GID
            if isinstance(user_item['id'], int):
                if user_item['id'] in ids:
                    raise AnsibleError('Group field must be unique: id')
                else:
                    ids.add(user_item['id'])
                    group['id'] = user_item['id']

            # State
            if 'state' in user_item:
                group['state'] = user_item['state']

            # Add item
            groups.append(group)
        return groups

    # Return user list with root user added
    def add_root_user(self, user_list, passwd):
        # Check password
        if not passwd.strip():
            raise AnsibleError('Root password can not be empty')
        # Set
        root_user = {}
        root_user['type'] = 'user'
        root_user['name'] = 'root'
        root_user['group'] = 'root'
        root_user['expires'] = float(-1)
        root_user['state'] = 'present'
        root_user['id'] = ''
        root_user['comment'] = ''
        root_user['shell'] = ''
        root_user['home'] = ''
        root_user['home_create'] = True
        root_user['home_move'] = True
        root_user['groups']=''
        root_user['sudo_permissions'] = []
        root_user['password'] = passwd
        root_user['password_update'] = 'always'
        root_user['ssh_directory'] = True
        root_user['ssh_known_hosts'] = []
        root_user['ssh_known_hosts_purge'] = False
        root_user['ssh_authorized_keys'] = []
        root_user['ssh_private_key_state'] = 'absent'
        root_user['ssh_private_key_generate'] = False
        root_user['ssh_private_key_content'] = ''
        root_user['ssh_private_key_type'] = ''
        root_user['ssh_private_key_bits'] = ''
        root_user['ssh_private_key_comment'] = ''
        root_user['ssh_private_key_file'] = ''
        root_user['ssh_private_key_path'] = ''
        root_user['ssh_private_key_authorized'] = False
        root_user['ssh_private_key_export'] = False

        user_list.append(root_user)
        return user_list

    # Return list of shell packages to install
    def shell_packages(self, user_list, parsing_vars):
        packages = []
        for user_item in user_list:
            for shell_name, shell_list in parsing_vars['shells']['uninstalled'].items():
                if user_item['shell'] == shell_list[0]:
                    shell_package = shell_list[1]
                    if shell_package not in packages:
                        packages.append(shell_package)
        return packages

    # Return list of ssh packages to install
    def needs_ssh_packages(self, user_list):
        install = False
        for user_item in user_list:
            if user_item['ssh_private_key_generate']:
                return True
        return False

    # Return public key from user_data
    def users_public_key(self, user_data, username):
        for user_item in user_data:
            if user_item['name'] == username:
                if 'ssh_public_key' in user_item:
                    return str(user_item['ssh_public_key'])
                else:
                    return ''

    # Return dict of exported keys
    def combine_export_keys(self, data_list):
        keys = {}
        for data in data_list:
            user = data['item']['name']
            if 'stdout' in data:
                if 'BEGIN OPENSSH PRIVATE KEY' in data['stdout']:
                    if not user in keys:
                        keys[user] = {}
                    keys[user]['private'] = data['stdout']
                    keys[user]['name'] = data['item']['ssh_private_key_file']
                else:
                    if not user in keys:
                        keys[user] = {}
                    keys[user]['public'] = data['stdout']
        return keys
