#!/usr/bin/python
# Filter plugins for data manipulation

class FilterModule(object):
    def filters(self):
        return {
            'domain_to_filename': self.domain_to_filename,
            'filename_to_domain': self.filename_to_domain
        }

    # Generate filename from domain name
    def domain_to_filename(self, domain):
        filename = domain
        filename = filename.replace("*","wildcard")
        filename = filename.replace(".","_")
        return filename

    # Generate domain name from filename
    def filename_to_domain(self, filename):
        domain = filename
        domain = domain.replace("wildcard","*")
        domain = domain.replace("_",".")
        return domain
