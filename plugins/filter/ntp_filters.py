#!/usr/bin/python
# Filter plugins for data manipulation

class FilterModule(object):
    def filters(self):
        return {
            'ntp_server_list': self.ntp_server_list,
            'ntp_peer_list': self.ntp_peer_list
        }

    # Return list servers from source
    def ntp_server_list(self, src):
        server_list = []
        if 'servers' in src:
            for server in src['servers']:
                if 'source' in server:
                    server_list.append(server['source'])
        return server_list

    # Return list peers from server
    def ntp_peer_list(self, src):
        peer_list = []
        if 'peers' in src:
            for peer in src['peers']:
                if 'source' in peer:
                    peer_list.append(peer['source'])
        return peer_list
