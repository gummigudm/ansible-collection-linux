#!/usr/bin/python
# Filter plugins for data manipulation

from ansible.errors import AnsibleError

class FilterModule(object):
    def filters(self):
        return {
            'vault_storage_validate': self.vault_storage_validate  #,
        }

    # Validate storage configuration
    def vault_storage_validate(self, storage):
        if 'type' in storage:
            if storage['type'] == 'file':
                pass
            elif storage['type'] == 'raft':
                pass
            elif storage['type'] == 'consul':
                pass
            else:
                raise AnsibleError('Storage field must be of type: file,raft,consul')
        else:
            raise AnsibleError('Storage field missing: type')
        if 'path' in storage and storage['path']:
            pass
        else:
            raise AnsibleError('Storage field missing: path')

    # Validate listener storage
    # def vault_storage_validate(self, storage):
    #     if 'type' in storage:
    #         if storage['type'] == 'file':
