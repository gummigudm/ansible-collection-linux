#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = r'''
---
module: gummigudm.linux.file

short_description: Creates or reads file

version_added: "1.0.0"

description: Creates or reads file on remote host and returns content.

options:
    path:
        description: Path to file.
        required: true
        type: str
    state:
        description: Shoudl file be present or absent
        required: false
        type: str

# Specify this value according to your collection
# in format of namespace.collection.doc_fragment_name
extends_documentation_fragment:
    - gummigudm.linux.file

author:
    - Gudmundur Gudmundsson (@gummigudm)
'''

EXAMPLES = r'''
# Pass in a message
- name: Generate or read token
  gummigudm.linux.file:
    state: read
    path: /dir/token
    owner:
    group:
    mode:
    content: '' # sets content if different
'''



RETURN = r'''
# These are examples of possible return values, and in general should use other names for return values.
token:
    description: The token string.
    type: str
    returned: always
    sample: 'hello world'
state:
    description: The output message that the test module generates.
    type: str
    returned: always
    sample: 'goodbye'
'''

from ansible.module_utils.basic import AnsibleModule
import string
import os

def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        state = dict(type='str', required=False, default='read',
            choices = ['present','absent','read']
        ),
        path = dict(type='str', required=True),
        content = dict(type='str', required=False),
    )

    result = dict(
        changed = False,
        exists = False,
        path = '',
        content = '',
    )

    module = AnsibleModule(
        argument_spec = module_args,
        supports_check_mode = True,
        add_file_common_args = True
    )

    # Set variables
    dir =  os.path.dirname(module.params['path'])
    result['path'] = module.params['path']

    # Action - read - stat
    if module.params['state'] == 'read':
        # If file does not exist, exit
        if not os.path.isfile(module.params['path']):
             module.exit_json(**result)
        result['exists'] = True
        # Read file contents
        try:
            with open(module.params['path'], "r") as f:
                result['content'] = f.read().rstrip()
        except:
            module.fail_json(msg='Could not read file', **result)
        # Exit
        module.exit_json(**result)

    # Action - Present - create
    if module.params['state'] == 'present':
        result['exists'] = True
        # If directory does not exist, fail
        if not os.path.isdir(dir):
            module.fail_json(msg='Directory for token does not exists.', **result)
        # If content is supplied, set result
        if module.params['content']:
            result['content'] = module.params['content'].rstrip()

        # If file does not exist, create
        if not os.path.isfile(module.params['path']):
            result['changed'] = True
            if module.check_mode:
                module.exit_json(**result)

            try:
                # Create file
                with open(module.params['path'], "w") as f:
                    f.write(result['content'].rstrip())
            except:
                module.fail_json(msg='Could not create file', **result)
        # If file exists, change
        else:
            try:
                # Change file
                with open(module.params['path'], "r+") as f:
                    current_content = f.read().rstrip()
                    if module.params['content']:
                        new_content = result['content']
                        if new_content != current_content:
                            f.seek(0)
                            f.write(new_content)
                            f.truncate()
                            result['changed'] = True
                    else:
                        result['content'] = current_content
            except:
                module.fail_json(msg='Could not change file', **result)
        # Set attributes
        file_args = module.load_file_common_arguments(module.params)
        result['changed'] = module.set_fs_attributes_if_different(file_args, result['changed'])
        # Exit
        module.exit_json(**result)

    # Action - Absent - Remove
    if os.path.isfile(module.params['path']):
        os.remove(module.params['path'])
        result['changed'] = True
    # Exit
    module.exit_json(**result)

def main():
    run_module()

if __name__ == '__main__':
    main()
