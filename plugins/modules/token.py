#!/usr/bin/env python3

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = r'''
---
module: gummigudm.linux.token

short_description: Reads, generates and saves tokens

version_added: "1.0.0"

description: Reads, generates and saves tokens on remote host and returns content. Token precedence is token, env, path, generated where one is required.

options:
    token:
        description: Passed token value.
        required: false
        type: str
    env:
        description: Name of environment variable on host to get token value from.
        required: false
        type: str
    path:
        description: Path to store token file.
        required: false
        type: str
    save:
        description: Should token be saved to path. Path is required if saved is true.
        required: false
        type: bool
        default: false
    generate:
        description: Passed token value, instead of generation.
        required: false
        type: bool
        default: true
    length:
        description: Token length to generate.
        required: false
        type: int
    format:
        description: Format of generated token.
        required: false
        type: str
        options: number,lower,upper,alpha,alnum,uppnum,lownum

extends_documentation_fragment:
    - gummigudm.linux.token
    - ansible.builtin.files

author:
    - Gudmundur Gudmundsson (@gummigudm)
'''

EXAMPLES = r'''
- name: Generate or read token
  gummigudm.linux.token:
    token: ''
    env: 'ENV_VAR_NAME'
    path: /dir/token
    save: true
    mode: 0600
    generate: true
    length: 24
    format: 'alnum'
'''

RETURN = r'''
value:
    description: The token string.
    type: str
    returned: always
    sample: 'token_value'
source:
    description: The output message that the test module generates.
    type: str
    returned: always
    sample: 'goodbye'
path:
    description: The file path for token read and write.
    type: str
    returned: If file action is performed
    sample: '/some/sample/dir/token_file'
'''

from ansible.module_utils.basic import AnsibleModule
import string
import os
from random import choice, shuffle

def run_module():
    module_args = dict(
        token=dict(type='str', required=False),
        env=dict(type='str', required=False),
        path=dict(type='str', required=False),
        save=dict(type='bool', required=False, default=False),
        generate=dict(type='bool', required=False, default=True),
        length=dict(type='int', required=False, default=32),
        format=dict(type='str', required=False, default='alnum', choices=['number','lower','upper','alpha','alnum','uppnum','lownum'])
    )

    result = dict(
        changed = False,
        source = '',
        path = '',
        value = ''
    )

    module = AnsibleModule(
        argument_spec=module_args,
        required_one_of=[('token', 'env', 'path', 'generate')],
        required_if = [('save', True, ('path',))],
        supports_check_mode=True,
        add_file_common_args=True
    )

    # Validation
    no_token_set = True

    # Set token
    if module.params['token']:
        result['value'] = module.params['token']
        result['source'] = 'variable'
        no_token_set = False

    if no_token_set and module.params['env']:
        if module.params['env'] in os.environ:
            result['value'] = os.environ.get( module.params['env'])
            result['source'] = 'environment'
            no_token_set = False

    if no_token_set and module.params['path']:
        path = os.path.expanduser(module.params['path'])

        if os.path.isfile(path):
            try:
                with open(path, "r") as f:
                    content = f.read()
                result['value'] = content.strip()
                result['source'] = 'file'
                result['path'] = path
                no_token_set = False
            except:
                module.fail_json(msg='Could not read token from file: ' + path, **result)

    if no_token_set and module.params['generate']:
        if module.params['length'] < 8 or module.params['length'] > 128:
            module.fail_json(msg='Token length must be between 8 and 128', **result)

        result['value'] = generate_token(module.params['length'], module.params['format'])
        result['source'] = 'generated'
        no_token_set = False
    
    if no_token_set:
        module.fail_json(msg='Token missing.', **result)

    # Save token
    if module.params['save']:
        path = os.path.expanduser(module.params['path'])
        dir =  os.path.dirname(path)

        if not os.path.isdir(dir):
            module.fail_json(msg='Directory for saving token does not exists.', **result)

        if not module.check_mode:
            if not os.path.isfile(path):
                try:
                    with open(path, "w") as f:
                        f.write(result['value'])
                        result['changed'] = True
                except:
                    module.fail_json(msg='Could not create token', **result)
            else:
                try:
                    with open(path, "r+") as f:
                        current_token =  f.read().rstrip()
                        if current_token != result['value']:
                            f.seek(0)
                            f.write(result['value'])
                            f.truncate()
                            result['changed'] = True
                except:
                    module.fail_json(msg='Could not overwrite token', **result)

        file_args = module.load_file_common_arguments(module.params)
        result['changed'] = module.set_fs_attributes_if_different(file_args, result['changed'])

    module.exit_json(**result)

def generate_token(length, format):
    token_string = ''
    chars = ''
    if format in ['number','lownum','uppnum','alnum']:
        chars = chars + string.digits
        token_string = token_string + choice(string.digits)
    if format in ['upper','uppnum','alpha','alnum']:
        chars = chars + string.ascii_uppercase
        token_string = token_string + choice(string.ascii_uppercase)
    if format in ['lower','lownum','alpha','alnum']:
        chars = chars + string.ascii_lowercase
        token_string = token_string + choice(string.ascii_lowercase)

    for _ in range(length - len(token_string)):
        token_string += ''.join(choice(chars))

    token_list = list(token_string)
    shuffle(token_list)
    token = ''.join(token_list)
    return token

def main():
    run_module()

if __name__ == '__main__':
    main()
